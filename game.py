from random import randint

# asks your name with the prompt "Hi! What is your name?\n"

# try to guess birth month and year with a prompt formatted like Guess
    # <<guess number>> : <<name>> were you born in <<m>> / <<yyyy>> ? then prompts with "yes or no?"

# If the computer guess correctly because you respond yes, it prints the message "I knew it!" and stops guessing

# If the computer guesses incorrectly because you respond no, 
    # it prints the message "Drat! Lemme try again!" up until 5 guesses, 
    # Otherwise, prints the message "I have other things to do. Goodbye."

name = input("What is your name?\n ")

for guess_number in range(1, 6):
    m = randint(1, 12)
    yyyy = randint(1924, 2004)

    print("Guess", guess_number, ":", "Hello,", name, "were you born in", m, "/", yyyy, "?")

    response = input("Yes or No?\n ")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Goodbye.")
    else:
        print("Drat! Lemme Try Again!")




